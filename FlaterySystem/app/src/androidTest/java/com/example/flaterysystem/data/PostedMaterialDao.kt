package com.example.flaterysystem.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.example.flaterysystem.utilities.*
import org.hamcrest.Matchers.equalTo
import org.junit.Assert.assertThat
import org.junit.After
import org.junit.Assert.assertNull
import com.example.flaterysystem.utilities.testMaterial
import com.example.flaterysystem.utilities.testMaterialsOf
import org.junit.Before
import org.junit.Rule
import org.junit.Test
class PostedMaterialDao {
    private lateinit var database: DatabaseMaterial
    private lateinit var postMaterialDao: postedMaterialDao
    private var testPostMaterialId: Long = 0

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        database = Room.inMemoryDatabaseBuilder(context,DatabaseMaterial::class.java).build()
        postMaterialDao = database.postedMaterialDao()

        database.materialDao().insertAll(testMaterialsOf)
        testPostMaterialId = postMaterialDao.insertPostedMaterial(testpostedMaterials)
    }

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun testGetPostMaterials() {
        val postMaterial2 = postedMaterial(
            testMaterialsOf[1].materialId,
            testMaterialsOf[1].name,
            testMaterialsOf[1].price,
            testCalendar
        )
        postMaterialDao.insertPostedMaterial(postMaterial2)
        assertThat(getValue(postMaterialDao.getPostedMaterial()).size, equalTo(2))
    }

    @Test
    fun testGetPostMaterial() {
        assertThat(
            getValue(postMaterialDao.getById(testPostMaterialId)),
            equalTo(testpostedMaterials)
        )
    }

    @Test
    fun testDeletePostMaterials() {
        val postMaterial2 = postedMaterial(
            testMaterialsOf[1].materialId,
            testMaterialsOf[1].name,
            testMaterialsOf[1].price,
            testCalendar
        )
        postMaterialDao.insertPostedMaterial(postMaterial2)
        assertThat(getValue(postMaterialDao.getPostedMaterial()).size, equalTo(2))
        postMaterialDao.deletePostedMaterial(postMaterial2)
        assertThat(getValue(postMaterialDao.getPostedMaterial()).size, equalTo(1))
    }

    @Test fun testGetPostMaterialForMaterial() {
        assertThat(getValue(postMaterialDao.findpostedMaterialForMaterial(testMaterial.materialId)),
            equalTo(testpostedMaterials))
    }

    @Test fun testGetPostMaterialForMaterial_notFound() {
        assertNull(getValue(postMaterialDao.findpostedMaterialForMaterial(testMaterialsOf[2].materialId)))
    }

    @Test fun testGetMaterialsAndPostMaterials() {
        val materialAndPostMaterials = getValue(postMaterialDao.getPostedMaterialAndMaterial())
        assertThat(materialAndPostMaterials.size, equalTo(3))

        /**
         * Only the [testPlant] has been planted, and thus has an associated [GardenPlanting]
         */
        assertThat(materialAndPostMaterials[0].material, equalTo(testMaterial))
        assertThat(materialAndPostMaterials[0].postedMaterials.size, equalTo(1))
        assertThat(materialAndPostMaterials[0].postedMaterials[0], equalTo(testpostedMaterials))

        // The other plants in the database have not been planted and thus have no GardenPlantings
        assertThat(materialAndPostMaterials[1].postedMaterials.size, equalTo(0))
        assertThat(materialAndPostMaterials[2].postedMaterials.size, equalTo(0))
    }
}