package com.example.flaterysystem.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class MaterialDaoTest {
    private lateinit var database: DatabaseMaterial
    private lateinit var materialDao: materialsDao
    private val materialA = materials("1", "A", 1.0, "",  "")
    private val materialB = materials("2", "B", 1.0, "",  "")
    private val materialC = materials("3", "C", 1.0, "",  "")

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        database = Room.inMemoryDatabaseBuilder(context, DatabaseMaterial::class.java).build()
        materialDao = database.materialDao()


        // Insert plants in non-alphabetical order to test that results are sorted by name
        materialDao.insertAll(listOf(materialB, materialC, materialA))
    }

    @After
    fun closeDb() {
        database.close()
    }



}