package com.example.flaterysystem.viewModels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.example.flaterysystem.data.DatabaseMaterial
import com.example.flaterysystem.data.materialRepository
import com.example.flaterysystem.data.postedMaterialRepository
import com.example.flaterysystem.utilities.getValue
import com.example.flaterysystem.utilities.testMaterial
import com.example.flaterysystem.viewModels.materialDetailViewModel
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.Assert.assertFalse

class MaterialDetailViewModel {
    private lateinit var appDatabase: DatabaseMaterial
    private lateinit var viewModel: materialDetailViewModel

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        appDatabase = Room.inMemoryDatabaseBuilder(context,DatabaseMaterial::class.java).build()

        val materialRepo = materialRepository.getInstance(appDatabase.materialDao())
        val postMaterialRepo = postedMaterialRepository.getInstance(
            appDatabase.postedMaterialDao())
        viewModel =materialDetailViewModel(materialRepo,postMaterialRepo, testMaterial.materialId, testMaterial.name,
            testMaterial.price)
    }

    @After
    fun tearDown() {
        appDatabase.close()
    }

    @Test
    @Throws(InterruptedException::class)
    fun testDefaultValues() {
        assertFalse(getValue(viewModel.isPosted))
    }
}