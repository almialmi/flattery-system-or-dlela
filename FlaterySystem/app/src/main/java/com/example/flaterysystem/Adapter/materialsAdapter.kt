package com.example.flaterysystem.Adapter


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil.bind
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.flaterysystem.ListMaterialFragmentDirections
import com.example.flaterysystem.data.materials
import com.example.flaterysystem.databinding.ListItemMaterialBinding

class materialsAdapter:ListAdapter<materials, materialsAdapter.ViewHolder>(PlantDiffCallback()) {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val material = getItem(position)
        holder.apply {
            bind(createOnClickListener(((material.materialId).toString())),material)
            itemView.tag =material
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
           ListItemMaterialBinding.inflate(
            LayoutInflater.from(parent.context), parent, false))
    }

    private fun createOnClickListener(materialtId: String): View.OnClickListener {
        return View.OnClickListener {
            val direction = ListMaterialFragmentDirections.actionMaterialListFragmentMaterialDetailFragment(materialId = "")
            it.findNavController().navigate(direction)
        }
    }

    class ViewHolder(
        private val binding: ListItemMaterialBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(listener: View.OnClickListener, item: materials) {
            binding.apply {
                clickListener=listener
                material = item
                executePendingBindings()
            }
        }
    }
}

private class PlantDiffCallback : DiffUtil.ItemCallback<materials>() {

    override fun areItemsTheSame(oldItem: materials, newItem: materials): Boolean {
        return oldItem.materialId == newItem.materialId
    }

    override fun areContentsTheSame(oldItem: materials, newItem: materials): Boolean {
        return oldItem == newItem
    }

}