package com.example.flaterysystem.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.flaterysystem.PostFragmentDirections
import com.example.flaterysystem.R
import com.example.flaterysystem.data.MaterialAndPostedMaterial
import com.example.flaterysystem.databinding.FragmentPostListBinding
import com.example.flaterysystem.viewModels.materialAndPostMaterialViewModel


class postedMaterialAdapter: ListAdapter<MaterialAndPostedMaterial, postedMaterialAdapter.ViewHolder>(PostMaterialDiffCallback()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.list_item_material, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let {material ->
            with(holder) {
                itemView.tag = material
                bind(createOnClickListener(material.material.materialId), material)
            }
        }
    }

    private fun createOnClickListener(materialId: String): View.OnClickListener {
        return View.OnClickListener {
            val direction =
                PostFragmentDirections.actionPostFragmentToDetailFragment(materialId)
            it.findNavController().navigate(direction)
        }
    }

    class ViewHolder(
        private val binding: FragmentPostListBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(listener: View.OnClickListener, material:MaterialAndPostedMaterial) {
            with(binding) {
                clickListener = listener
                viewModel = materialAndPostMaterialViewModel(material)
                executePendingBindings()
            }
        }
    }
}

private class PostMaterialDiffCallback : DiffUtil.ItemCallback<MaterialAndPostedMaterial>() {

    override fun areItemsTheSame(
        oldItem: MaterialAndPostedMaterial,
        newItem: MaterialAndPostedMaterial
    ): Boolean {
        return oldItem.material.materialId == newItem.material.materialId
    }

    override fun areContentsTheSame(
        oldItem: MaterialAndPostedMaterial,
        newItem: MaterialAndPostedMaterial
    ): Boolean {
        return oldItem.material == newItem.material
    }

}