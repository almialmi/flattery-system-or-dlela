package com.example.flaterysystem

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.flaterysystem.data.materials
import com.example.flaterysystem.viewModels.materialDetailViewModel
import com.example.flaterysystem.viewModels.materialDetailViewModelFactory
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_add.*
import kotlinx.android.synthetic.main.fragment_add.view.*
import kotlinx.android.synthetic.main.fragment_login.*
import kotlinx.android.synthetic.main.fragment_login.view.*

class AddFragment : Fragment() {
    private lateinit var name:EditText
    private lateinit var image:EditText
    private lateinit var price:EditText
    private lateinit var desc:EditText
   lateinit var materialViewModel:materialDetailViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        materialViewModel=ViewModelProviders.of(this).get(materialDetailViewModel::class.java)
        // Inflate the layout for this fragment
       val view = inflater.inflate(R.layout.fragment_add, container, false)
         name =view.name_edit
        image = view.image_edit
         price = view.price_edit
         desc=view.desc_edit
        val Add =view.add_btn


        Add.setOnClickListener {
            val material=readField()
            materialViewModel.addMaterialToPostMaterial()
            Snackbar.make(view, R.string.addedMaterial, Snackbar.LENGTH_LONG).show()
        }

        return view
    }
    private fun readField()=materials(name.text.toString(),image.text.toString(),price.text.toString().toDouble(),desc.text.toString())



}
