package com.example.flaterysystem

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.flaterysystem.data.User
import com.example.flaterysystem.viewModels.UserViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_apply.view.*
import kotlinx.android.synthetic.main.fragment_detail.view.*

class ApplyFragment : Fragment() {
    lateinit var viewModel:UserViewModel
    lateinit var materialName: EditText
    lateinit var name:EditText
    lateinit var phoneNumber:EditText
    lateinit var applyButton:Button
    private var user:List<User> = emptyList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel=ViewModelProviders.of(this).get(UserViewModel::class.java)
        // Inflate the layout for this fragment
       val view= inflater.inflate(R.layout.fragment_apply, container, false)
      materialName=view.mater_name
        name=view.per_name
        phoneNumber=view.phone
        applyButton=view.apply_btn1
        applyButton.setOnClickListener {
          val user=  readField()
            viewModel.inserUser(user)
            Snackbar.make(view, R.string.request_notify, Snackbar.LENGTH_LONG).show()

        }




    return view
    }
    fun readField()= User(materialName.text.toString(),name.text.toString(),phoneNumber.text.toString())



}
