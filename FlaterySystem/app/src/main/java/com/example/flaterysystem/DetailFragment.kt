package com.example.flaterysystem

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.Button
import androidx.core.app.ShareCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.navigation.fragment.navArgs
import com.example.flaterysystem.Adapter.materialsAdapter
import com.example.flaterysystem.data.materials
import com.example.flaterysystem.utilities.InjectorUtils
import com.example.flaterysystem.viewModels.materialDetailViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_detail.*
import com.example.flaterysystem.databinding.FragmentDetailBinding
import com.example.flaterysystem.databinding.ListMaterialFragmentBinding

class DetailFragment: Fragment() {
    lateinit var Request:Button
    private val args: DetailFragmentArgs by navArgs()
    private lateinit var shareText: String

    private lateinit var listener: onRequestButtonClicked
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is onRequestButtonClicked){
            listener=context
        }
    }

    private val materialDetailViewModel: materialDetailViewModel by viewModels {
        InjectorUtils.provideMaterialDetailViewModelFactory(requireActivity(),args.materialId,0.0,"")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentDetailBinding>(
            inflater, R.layout.fragment_detail, container, false).apply {
            viewModel = materialDetailViewModel
            lifecycleOwner = this@DetailFragment
            fab.setOnClickListener { view ->
                materialDetailViewModel.addMaterialToPostMaterial()
                Snackbar.make(view, R.string.addedMaterial, Snackbar.LENGTH_LONG).show()
            }
            Request =apply_btn
            Request.setOnClickListener {
                     listener.onRequestButtonClicked()
            }

        }

        materialDetailViewModel.material.observe(this) { post ->
            shareText = if (post == null) {
                ""
            } else {
                "shared"
            }
        }

        setHasOptionsMenu(true)

        return binding.root
    }



    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_material_detail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    @Suppress("DEPRECATION")
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_share -> {
                val shareIntent = ShareCompat.IntentBuilder.from(activity)
                    .setText(shareText)
                    .setType("text/plain")
                    .createChooserIntent()
                    .apply {
                        // https://android-developers.googleblog.com/2012/02/share-with-intents.html
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            // If we're on Lollipop, we can open the intent as a document
                            addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT or Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
                        } else {
                            // Else, we will use the old CLEAR_WHEN_TASK_RESET flag
                            addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)
                        }
                    }
                startActivity(shareIntent)
                return true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
    interface onRequestButtonClicked{
        fun onRequestButtonClicked()
    }
}