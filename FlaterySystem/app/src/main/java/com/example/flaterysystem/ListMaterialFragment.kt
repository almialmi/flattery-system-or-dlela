package com.example.flaterysystem
import android.content.res.Configuration
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import androidx.room.Transaction
import com.example.flaterysystem.Adapter.materialsAdapter
import com.example.flaterysystem.utilities.InjectorUtils
import com.example.flaterysystem.viewModels.materialListViewModel
import com.example.flaterysystem.databinding.ListMaterialFragmentBinding

class ListMaterialFragment : Fragment() {

    private val viewModel: materialListViewModel by viewModels {
        InjectorUtils.provideMaterialListViewModelFactory(requireContext())
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)
       //  val binding = inflater.inflate(R.layout.list_material_fragment,container,false)

        val binding = ListMaterialFragmentBinding.inflate(inflater, container, false)
            context ?: return binding.root
            val adapter = materialsAdapter()
            binding.materialList.adapter = adapter
            subscribeUi(adapter)

            setHasOptionsMenu(true)
            return binding.root
        }

        override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
            inflater.inflate(R.menu.menu_material_detail, menu)
        }

        override fun onOptionsItemSelected(item: MenuItem): Boolean {
            return when (item.itemId) {
                R.id.new_material-> {
                    item.setActionView(R.layout.fragment_add)
                    return  true
                }
                else -> super.onOptionsItemSelected(item)
            }
        }

        private fun subscribeUi(adapter: materialsAdapter) {
            viewModel.material.observe(viewLifecycleOwner) { post ->
                /**
                 *  Plant may return null, but the [observe] extension function assumes it will not be null.
                 *  So there will be a warning（Condition `plants != null` is always `true`） here.
                 *  I am not sure if the database return data type should be defined as nullable, Such as `LiveData<List<Plant>?>` .
                 */
                if (post != null) adapter.submitList(post)
            }
        }


    }