package com.example.flaterysystem

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import com.example.flaterysystem.Adapter.postedMaterialAdapter
import com.example.flaterysystem.data.postedMaterial
import com.example.flaterysystem.utilities.InjectorUtils
import com.example.flaterysystem.viewModels.postedMaterialListViewModel
import com.example.flaterysystem.databinding.FragmentPostBinding

class PostFragment:Fragment() {
    private val viewModel: postedMaterialListViewModel by viewModels {
        InjectorUtils. providePostedMaterialListViewModelFactory(requireContext())
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding =FragmentPostBinding.inflate(inflater, container, false)
        val adapter = postedMaterialAdapter()
        binding.postList.adapter = adapter
        subscribeUi(adapter, binding)
        return binding.root
    }
    private fun subscribeUi(adapter: postedMaterialAdapter, binding: FragmentPostBinding) {
        viewModel.postedMaterial.observe(viewLifecycleOwner) { material->
            binding.hasMaterials = !material.isNullOrEmpty()
        }

        viewModel.materialAndPostedMaterials.observe(viewLifecycleOwner) { result ->
            if (!result.isNullOrEmpty())
                adapter.submitList(result)
        }
    }

}