package com.example.flaterysystem.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.example.flaterysystem.utilities.DATABASE_NAME
import com.example.flaterysystem.worker.DatabaseWorker

@Database(entities = [materials::class,postedMaterial::class],version = 1,exportSchema = false)
@TypeConverters(Converters::class)
abstract class DatabaseMaterial:RoomDatabase(){
    abstract fun postedMaterialDao():postedMaterialDao
    abstract fun materialDao():materialsDao

    companion object {
        @Volatile private var instance:DatabaseMaterial?=null
        fun getInstance(context:Context):DatabaseMaterial{
            return instance?: synchronized(this){
                instance ?:buildDatabase(context).also{ instance=it}
            }
        }

        private fun buildDatabase(context: Context): DatabaseMaterial {
            return Room.databaseBuilder(context,DatabaseMaterial::class.java,DATABASE_NAME)
                .addCallback(object : RoomDatabase.Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        val request= OneTimeWorkRequestBuilder<DatabaseWorker>().build()
                        WorkManager.getInstance(context).enqueue(request)
                    }

                }).build()
        }
    }
}
