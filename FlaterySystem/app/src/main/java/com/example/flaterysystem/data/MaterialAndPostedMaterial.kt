package com.example.flaterysystem.data

import androidx.room.Embedded
import androidx.room.Relation

class MaterialAndPostedMaterial {
    @Embedded
    lateinit var material:materials
    @Relation(parentColumn = "id",entityColumn = "m_id")
    var postedMaterials:List<postedMaterial> = arrayListOf()
}