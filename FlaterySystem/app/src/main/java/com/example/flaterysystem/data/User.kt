package com.example.flaterysystem.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import java.io.Serializable

@Entity(tableName = "user")
data class User(@ColumnInfo(name="material_name") val materialName:String,
                @ColumnInfo(name="user_name") val name:String,
                @ColumnInfo(name="phone_number")val phoneNo:String):Serializable