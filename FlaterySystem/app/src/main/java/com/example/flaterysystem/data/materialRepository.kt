package com.example.flaterysystem.data

class materialRepository private constructor (private val materials_dao: materialsDao){
    fun getMaterials()=materials_dao.getMaterials()
    fun getMaterialById(id:String)=materials_dao.findById(id)
    fun getMaterialByName(name:String)=materials_dao.findByName(name)
    fun insertMateral(material: List<materials>)=materials_dao.insertAll(material)

    companion object{
        @Volatile private var instance:materialRepository?=null
        fun getInstance(materials_dao: materialsDao)=
                instance?: synchronized(this){
                    instance?:materialRepository(materials_dao).also { instance=it }
                }
    }
}