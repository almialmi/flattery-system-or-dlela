package com.example.flaterysystem.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.UUID

@Entity(tableName = "material")
data class materials (@PrimaryKey @ColumnInfo(name="id")val materialId:String,
                      @ColumnInfo(name="material_name") val name:String,
                      val price:Double,val description:String,val imageUrl:String="")