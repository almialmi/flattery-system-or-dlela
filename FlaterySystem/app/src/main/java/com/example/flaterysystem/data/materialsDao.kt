package com.example.flaterysystem.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface materialsDao {
    @Query("SELECT * FROM material ORDER BY id")
    fun getMaterials():LiveData<List<materials>>

    @Query("SELECT * FROM material WHERE material_name= :name")
    fun findByName(name: String):LiveData<materials>

    @Query("SELECT * FROM material WHERE id= :id")
    fun findById(id: String):LiveData<materials>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(materials: List<materials>)

}