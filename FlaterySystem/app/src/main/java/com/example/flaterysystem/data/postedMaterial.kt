package com.example.flaterysystem.data

import androidx.room.*
import java.util.*

@Entity(tableName = "postedMaterial",
    foreignKeys = [ForeignKey(entity = materials::class,parentColumns =["id"],childColumns = ["m_id"],onDelete = ForeignKey.CASCADE)],
    indices = [Index("m_id")])
data class postedMaterial (@PrimaryKey @ColumnInfo(name="m_id")val materialId:String,
                           @ColumnInfo(name="name") val name:String,
                           @ColumnInfo(name="price") val price:Double,
                           @ColumnInfo(name="post_date") val date:Calendar= Calendar.getInstance())