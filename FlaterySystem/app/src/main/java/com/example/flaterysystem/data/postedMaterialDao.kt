package com.example.flaterysystem.data

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface postedMaterialDao {
    @Query("SELECT * FROM postedMaterial ")
    fun getPostedMaterial():LiveData<List<postedMaterial>>

    @Query("SELECT * FROM postedMaterial WHERE  m_id = :postmaterialId ")
    fun findpostedMaterialForMaterial(postmaterialId:String):LiveData<postedMaterial>

    @Query("SELECT * FROM postedMaterial WHERE m_id= :postId")
    fun getById(postId: Long):LiveData<postedMaterial>
    @Transaction
    @Query("SELECT * FROM material")
    fun getPostedMaterialAndMaterial():LiveData<List<MaterialAndPostedMaterial>>
    @Insert
    fun insertPostedMaterial(posted_material: postedMaterial):Long
    @Delete
    fun deletePostedMaterial(posted_material: postedMaterial)
}