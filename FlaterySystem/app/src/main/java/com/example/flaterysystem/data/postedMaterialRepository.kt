package com.example.flaterysystem.data

import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

class postedMaterialRepository(val posted_material_dao:postedMaterialDao) {

    suspend fun createPostMaterial(id:String,name:String,price:Double){
        withContext(IO){
            val posted=postedMaterial(id,name,price)
            posted_material_dao.insertPostedMaterial(posted)
        }
    }
    suspend fun removePostedMaterial( posted_material:postedMaterial){
        withContext(IO){
            posted_material_dao.deletePostedMaterial(posted_material)
        }
    }
    fun getPostedMaterialForMaterial(id:String)=posted_material_dao.findpostedMaterialForMaterial(id)
    fun getPostedMaterial()=posted_material_dao.getPostedMaterial()
    fun getPostedMaterialAndMaterial()=posted_material_dao.getPostedMaterialAndMaterial()

    companion object{
        @Volatile private var instance:postedMaterialRepository?=null

        fun getInstance(posted_material_dao: postedMaterialDao)=
                instance?: synchronized(this){
                    instance?:postedMaterialRepository(posted_material_dao).also { instance= it }
                }
    }

}