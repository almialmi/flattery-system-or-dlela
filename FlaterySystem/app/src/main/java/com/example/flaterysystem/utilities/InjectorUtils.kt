/*
 * Copyright 2018 Google LLC
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.flaterysystem.utilities

import android.content.Context
import com.example.flaterysystem.data.DatabaseMaterial
import com.example.flaterysystem.data.materialRepository
import com.example.flaterysystem.data.postedMaterial
import com.example.flaterysystem.data.postedMaterialRepository
import com.example.flaterysystem.viewModels.materialDetailViewModelFactory
import com.example.flaterysystem.viewModels.materialListViewModelFactory
import com.example.flaterysystem.viewModels.postedMaterialListViewModelFactory

/**
 * Static methods used to inject classes needed for various Activities and Fragments.
 */
object InjectorUtils {

    private fun getMaterialRepository(context: Context): materialRepository {
        return materialRepository.getInstance(
                DatabaseMaterial.getInstance(context.applicationContext).materialDao())
    }

    private fun getPostedMaterialRepository(context: Context): postedMaterialRepository {
        return postedMaterialRepository.getInstance(
                DatabaseMaterial.getInstance(context.applicationContext).postedMaterialDao())
    }

    fun providePostedMaterialListViewModelFactory(
        context: Context
    ): postedMaterialListViewModelFactory {
        val repository = getPostedMaterialRepository(context)
        return postedMaterialListViewModelFactory(repository)
    }

    fun provideMaterialListViewModelFactory(context: Context): materialListViewModelFactory {
        val repository = getMaterialRepository(context)
        return materialListViewModelFactory(repository)
    }

    fun provideMaterialDetailViewModelFactory(
        context: Context,
        materialId: String,
        price :Double,
        name:String
    ): materialDetailViewModelFactory {
        return materialDetailViewModelFactory(getMaterialRepository(context),
                getPostedMaterialRepository(context), materialId,name, price)
    }
}
