package com.example.flaterysystem.viewModels

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.ViewModel
import com.example.flaterysystem.data.MaterialAndPostedMaterial
import java.text.SimpleDateFormat
import java.util.*

class materialAndPostMaterialViewModel(postings: MaterialAndPostedMaterial) : ViewModel() {


    private val post = checkNotNull(postings.material)
    private val materialPosting = postings.postedMaterials[0]
    private val dateFormat by lazy { SimpleDateFormat("MMM d, yyyy", Locale.US) }
    val imageUrl = ObservableField<String>(post.imageUrl)
    val postName = ObservableField<String>(post.name)
    val postPrice = ObservableField<Double>(post.price)
    val postDateString = ObservableField<String>(dateFormat.format(materialPosting.date.time))


}