package com.example.flaterysystem.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.example.flaterysystem.data.materialRepository
import com.example.flaterysystem.data.materials
import com.example.flaterysystem.data.postedMaterialRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class materialDetailViewModel( private val materialRepository: materialRepository,
                             private val postedMaterialRepository: postedMaterialRepository,
                              private val materialId:String,private val name:String,private val price:Double):ViewModel() {
    var isPosted:LiveData<Boolean>
    val material:LiveData<materials>

    @ExperimentalCoroutinesApi
    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }
    init {
        val postMaterialForMaterial=postedMaterialRepository.getPostedMaterialForMaterial(materialId)
        isPosted=postMaterialForMaterial.map { it !=null }
        material=materialRepository.getMaterialById(materialId)
    }
    fun addMaterialToPostMaterial(){
        viewModelScope.launch {
            postedMaterialRepository.createPostMaterial(materialId,name, price)
        }
    }
}