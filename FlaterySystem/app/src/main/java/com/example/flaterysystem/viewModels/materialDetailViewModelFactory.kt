package com.example.flaterysystem.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.flaterysystem.data.materialRepository
import com.example.flaterysystem.data.postedMaterialRepository

class materialDetailViewModelFactory(private val materialRepository: materialRepository,
                                     private val postedMaterialRepository: postedMaterialRepository,
                                     private val materialId:String,
                                     private val name:String,private val price:Double):ViewModelProvider.NewInstanceFactory() {
    @Suppress
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return materialDetailViewModel(materialRepository,postedMaterialRepository,materialId,name, price) as T
    }
}