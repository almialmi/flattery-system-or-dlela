package com.example.flaterysystem.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.flaterysystem.data.materialRepository
import com.example.flaterysystem.data.materials

class materialListViewModel internal constructor (val materialRepository: materialRepository):ViewModel() {
    val material:LiveData<List<materials>> = materialRepository.getMaterials()

}