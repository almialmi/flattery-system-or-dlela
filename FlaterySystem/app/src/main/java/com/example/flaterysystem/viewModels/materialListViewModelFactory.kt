package com.example.flaterysystem.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.flaterysystem.data.materialRepository

class materialListViewModelFactory (private val material:materialRepository):ViewModelProvider.NewInstanceFactory(){
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>)=materialListViewModel(material) as T
}