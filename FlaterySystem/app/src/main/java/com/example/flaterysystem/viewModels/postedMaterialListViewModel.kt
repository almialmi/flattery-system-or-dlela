package com.example.flaterysystem.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.example.flaterysystem.data.MaterialAndPostedMaterial
import com.example.flaterysystem.data.postedMaterialRepository

class postedMaterialListViewModel internal constructor(
    postedMaterialRepository: postedMaterialRepository
) : ViewModel() {

    val postedMaterial =  postedMaterialRepository.getPostedMaterial()

    val materialAndPostedMaterials: LiveData<List<MaterialAndPostedMaterial>> =
        postedMaterialRepository.getPostedMaterialAndMaterial().map { posted ->
            posted.filter { it.postedMaterials.isNotEmpty() }
        }
}