package com.example.flaterysystem.viewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.flaterysystem.data.postedMaterialRepository

class postedMaterialListViewModelFactory(
    private val repository:postedMaterialRepository
) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return postedMaterialListViewModel(repository) as T
    }
}