package com.example.flaterysystem.worker

import android.content.Context
import com.google.gson.stream.JsonReader
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.example.flaterysystem.data.DatabaseMaterial
import com.example.flaterysystem.data.materials
import com.example.flaterysystem.utilities.MATERIAL_DATA_FILENAME
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.coroutineScope

class DatabaseWorker(c:Context,w:WorkerParameters):CoroutineWorker(c,w) {
    private val TAG by lazy { DatabaseWorker::class.java.simpleName }
    override suspend fun doWork(): Result = coroutineScope{
        try{
            applicationContext.assets.open(MATERIAL_DATA_FILENAME).use { inputStream ->
                JsonReader(inputStream.reader()).use { jsonReader->
                    val materialType=object:TypeToken<List<materials>>() {}.type
                    val materialList:List<materials> = Gson().fromJson(jsonReader,materialType)

                    val database=DatabaseMaterial.getInstance(applicationContext)
                    database.materialDao().insertAll(materialList)
                    Result.success()
                }
            }
        }  catch(ex:Exception){
            Log.e(TAG,"Error database occur",ex)
            Result.failure()
        }


    }
}

